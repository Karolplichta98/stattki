//
// Created by karol on 22.06.2020.
//

#ifndef STATKI_PLAYER_H
#define STATKI_PLAYER_H
#include "statkiboard.h"
#include "TextView.h"
#include <iostream>

class Player {
    TextView & view ;
    statkiboard & board;
public:
  Player(statkiboard & board, TextView & view) ;
 void play();
 void SetYourBoard() const;
};


#endif //STATKI_PLAYER_H