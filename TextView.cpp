//
// Created by karol on 22.06.2020.
//

#include "TextView.h"
#include <iostream>
using namespace std;
void TextView::display1() {
    for (int i = 0; i < board.getBoardHeight() ; ++i) {
        for (int j = 0; j < board.getBoardWidth(); ++j) {
            std::cout<<"[";
            std::cout<<board.showMyBoard(j,i);
            std::cout<<"]";
        }
        std::cout<<std::endl;

    }
    std::cout<<std::endl;
}
void TextView::display2() {
    for (int i = 0; i < board.getBoardHeight() ; ++i) {
        for (int j = 0; j < board.getBoardWidth(); ++j) {
            std::cout<<"[";
            std::cout<<board.showEnemyBoard(j,i);
            std::cout<<"]";
        }
        std::cout<<std::endl;

    }
    std::cout<<std::endl;
}
TextView::TextView(statkiboard &board) : board(board) {}