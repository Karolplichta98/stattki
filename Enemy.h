//
// Created by karol on 25.06.2020.
//

#ifndef STATKI_ENEMY_H
#define STATKI_ENEMY_H

#include "statkiboard.h"
#include <iostream>
#include "TextView.h"
class Enemy {
    TextView & view ;
    statkiboard & board;
public:
    Enemy(statkiboard & board, TextView & view) ;
    void enemyplay();
    void SetenemyBoard() const;
};



#endif //STATKI_ENEMY_H
