//
// Created by c on 22.05.19.
//

#ifndef STATKI_STATKIBOARD_H
#define STATKI_STATKIBOARD_H

enum GameState { Preperation,RUNNING ,FINISHED };

struct Field {
    bool hasShip;

    bool isShooted;
bool isNearShip;

};
class statkiboard {
    Field board[100][100];
    int width;
    int height;
public:
    statkiboard();
    statkiboard(int boardwidth,int boardheight);
    void debug_display() const;
    void setBoard();
    void setField(int width,int height,bool hasShip,bool isShooted );
    bool isRightField(int x,int y) const ;
    bool hasShip(int x,int y);
    bool isShot(int x,int y);
    void shoot(int x,int y);
GameState getGameState() const;
int countShips() const;
    void setShip1(int x,int y) ;
int countShotShips() const;
    void setShip3Vertically(int x,int y);
    void setShip3Horrizontally(int x,int y);
void ShipIsNear();
    void setShip5Vertically(int x,int y);
    void setShip5Horrizontally(int x,int y);
char showMyBoard(int x,int y) const ;
    char showEnemyBoard(int x,int y) const;
    int getBoardWidth() const ;
    int getBoardHeight()const;

    bool isNearShip(int x ,int y) const;
};


#endif //STATKI_STATKIBOARD_H