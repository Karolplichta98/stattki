//
// Created by c on 22.05.19.
//

#include "statkiboard.h"
#include <iostream>
using namespace std;
void statkiboard::debug_display() const {
    for (int c = 0; c < width; c++) {
        for (int i = 0; i < height; i++) {
            std::cout << "[";
            if (board[i][c].hasShip) {
                std::cout << "S";
            } else {
                std::cout << ".";
            }
            if (board[i][c].isShooted) {
                std::cout << "X";
            } else {
                std::cout << ".";
            }
            if (board[i][c].isNearShip) {
                std::cout << "N";
            } else {
                std::cout << ".";
            }

            std::cout << "]";
        }
        std::cout << std::endl;
    }
}

void statkiboard::shoot(int x, int y) {
    board[y][x].isShooted=1;
}
bool statkiboard::isShot(int x, int y) {
    if (board[y][x].isShooted==1) {
        return 1;
    }
}


bool statkiboard::isRightField(int x,int y) const  {
    if (x > width-1 || y > height-1 || x<0 || y<0) {

        return 0;
    } else{

        return 1;
    }
}

bool statkiboard::hasShip(int y, int x) {
    if (board[x][y].hasShip) {
        return 1;
    }
}


void statkiboard::setShip1(int x,int y) {

if (isRightField(x,y)==1 && board[x][y].isNearShip==0)
    {
        board[x][y].hasShip=1;
        ShipIsNear();

    }
}

void statkiboard::setShip3Vertically(int x,int y) {
    if(isRightField(x,y+1)==1 && isRightField(x,y-1)==1 && board[x][y+1].isNearShip==0 &&
                                 board[x][y-1].isNearShip==0  && board[x][y].isNearShip==0)
    {
        board[x][y].hasShip = 1;
        board[x][y + 1].hasShip = 1;
        board[x][y - 1].hasShip = 1;
        ShipIsNear();
    }
}
void statkiboard::setShip3Horrizontally(int x, int y)  {
    if(isRightField(x+1,y)==1 && isRightField(x-1,y)==1 &&
                                 board[x-1][y].isNearShip==0 && board[x+1][y].isNearShip==0 && board[x][y].isNearShip==0)
    {
        board[x][y].hasShip = 1;
        board[x+1][y].hasShip = 1;
        board[x-1][y].hasShip = 1;
        ShipIsNear();
    }
}
void statkiboard:: setShip5Vertically(int x,int y) {
    if(isRightField(x,y+2)==1 && isRightField(x,y-2)==1 && board[x][y-2].isNearShip==0 && board[x][y+2].isNearShip==0 && board[x][y+1].isNearShip==0 &&
                                 board[x][y-1].isNearShip==0  && board[x][y].isNearShip==0)
    {
        board[x][y].hasShip = 1;
        board[x][y + 1].hasShip = 1;
        board[x][y - 1].hasShip = 1;
        board[x][y + 2].hasShip = 1;
        board[x][y - 2].hasShip = 1;
        ShipIsNear();
    }
}
void statkiboard:: setShip5Horrizontally(int x,int y) {
    if(isRightField(x+2,y)==1 && isRightField(x-2,y)==1 && board[x+2][y].isNearShip==0 && board[x-2][y].isNearShip==0 &&
    board[x-1][y].isNearShip==0 && board[x+1][y].isNearShip==0 && board[x][y].isNearShip==0)
    {
        board[x+1][y].hasShip = 1;
        board[x-1][y].hasShip = 1;
        board[x+2][y].hasShip = 1;
        board[x-2][y ].hasShip = 1;
        board[x][y].hasShip = 1;
        ShipIsNear();
    }
}
statkiboard::statkiboard(int x, int y) {

    width = x;
    height = y;
    setBoard();
}
statkiboard::statkiboard() {
    width=10;
    height=10;
}
void statkiboard::setBoard() {
    for (int i = 0; i < width; i++) {
        for (int j = 0; j < height; j++) {
            setField(i,j,false,false);

        }
    }
}
void statkiboard::setField(int width, int height, bool hasShip, bool isShooted ) {
    for (int i = 0; i <=width ;i++) {
        for (int j = 0; j <=height ; j++) {
            board[width][height]={hasShip,isShooted};
        }
    }
}
int statkiboard::countShotShips() const{
    int ShotShips=0;
    for (int i = 0; i < width ;  ++i) {
        for (int j = 0; j < height;  ++j) {
            if ( board[i][j].isShooted==1 ) {
                ShotShips=ShotShips+1;
}}}
    return ShotShips;}
int statkiboard::countShips() const{
    int statki=0;
    for (int i = 0; i < width ;  ++i) {
        for (int j = 0; j < height;  ++j) {
if (board[j][i].hasShip==1) {
    statki++;
}
        }
    }
    return statki;

}
GameState statkiboard::getGameState() const {
    GameState state=Preperation;
state=Preperation;
    for (int x = 0; x < width; ++x) {
        for (int m = 0; m < height; ++m) {
            if (countShips()==27) {
                state=RUNNING;

                return state;
            }
            if (countShips() == countShotShips() && countShotShips()!=0) {
                state=FINISHED;
                cout<<"Finished";
                return state;
            }
        }
    }
}

void statkiboard::ShipIsNear() {
    for (int i = 0; i < width; ++i) {
        for (int j = 0; j <height ; ++j) {
            if (board[j][i].hasShip==1) {
                board[j][i+1].isNearShip=1;
                board[j][i-1].isNearShip=1;
                board[j+1][i].isNearShip=1;
                board[j-1][i].isNearShip=1;
            }
        }
    }

}

char statkiboard::showEnemyBoard(int x, int y) const {
    if (board[y][x].hasShip && board[y][x].isShooted && isRightField(x,y)==1) {
        return 'X';

    }
    if (!board[y][x].hasShip && board[y][x].isShooted) {
        return 'o';;
}
    if (board[y][x].isShooted==0) {
    return ' ';}
}

char statkiboard::showMyBoard(int x,int y) const {
    if (isRightField(x,y)==0 ) {
        return'#';
    }
    if (board[y][x].hasShip ) {
        return 'S';

    }
    if (board[y][x].hasShip && board[y][x].isShooted && isRightField(x,y)==1) {
        return 'X';

    }
    if (!board[y][x].hasShip && board[y][x].isShooted) {
        return 'o';
    }
    if (board[y][x].isNearShip && isRightField(x,y)==1) {
        return 'N';
    }
    if (board[y][x].isNearShip==0 && board[y][x].isShooted==0 && board[y][x].hasShip==0) {
        return ' ';
    }

}
int statkiboard::getBoardWidth() const {
    return width;
};
int statkiboard::getBoardHeight() const {
    return height;}




bool statkiboard::isNearShip(int x, int y) const {
    if (board[x][y].isNearShip==1) {
        return 1;
    } else {
        return 0;
    }
}